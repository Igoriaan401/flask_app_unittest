from flask import Flask, render_template, request
import math

app = Flask(__name__)


@app.route('/')
@app.route('/index/')
def index():
    return render_template('index.html')


@app.route('/', methods=['post', 'get'])
def form():
    if request.method == 'POST':
        a = int(request.form.get('a'))
        b = int(request.form.get('b'))
        c = int(request.form.get('c'))
        data = []
        D = b**2 - 4 * a * c
        try:
            if D < 0:
                data = otris()
            elif D == 0:
                data = ravno(b, a)
            else:
                data = bolshe(b, D, a)
        except:
            data = ["Error value"]
        return render_template('index.html', data=data)


def otris():
    res = "Нет корнет"
    return [
        "Результат: {res}".format(res=res)
    ]


def ravno(b, a):
    res = -b / 2 * a
    return [
        "Результат: {res}".format(res=res)
    ]


def bolshe(b, D, a):
    res1 = (-b + math.sqrt(D)) / (2 * a)
    res2 = (-b - math.sqrt(D)) / (2 * a)
    return "Результат: {res1}, {res2}".format(res1=res1, res2=res2)


if __name__ == '__main__':
    app.run()
